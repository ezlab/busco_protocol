# BUSCO version is: 5.2.2 
# The lineage dataset is: archaea_odb10 (Creation date: 2021-02-23, number of genomes: 404, number of BUSCOs: 194)
# Summarized benchmarking in BUSCO notation for file /data/manni/BUSCO_PROTOCOL/busco_protocol/alternate_protocol1/genomes_mix/GCF_000214415.1.fna
# BUSCO was run in mode: genome
# Gene predictor used: prodigal

	***** Results: *****

	C:99.5%[S:99.5%,D:0.0%],F:0.5%,M:0.0%,n:194	   
	193	Complete BUSCOs (C)			   
	193	Complete and single-copy BUSCOs (S)	   
	0	Complete and duplicated BUSCOs (D)	   
	1	Fragmented BUSCOs (F)			   
	0	Missing BUSCOs (M)			   
	194	Total BUSCO groups searched		   

Dependencies and versions:
	hmmsearch: 3.1
	prodigal: 2.6.3
