# BUSCO version is: 5.2.2 
# The lineage dataset is: methanococcales_odb10 (Creation date: 2021-02-23, number of genomes: 18, number of BUSCOs: 958)
# Summarized benchmarking in BUSCO notation for file /data/manni/BUSCO_PROTOCOL/busco_protocol/alternate_protocol1/genomes_mix/GCF_000214415.1.fna
# BUSCO was run in mode: genome
# Gene predictor used: prodigal

	***** Results: *****

	C:99.1%[S:98.4%,D:0.7%],F:0.0%,M:0.9%,n:958	   
	950	Complete BUSCOs (C)			   
	943	Complete and single-copy BUSCOs (S)	   
	7	Complete and duplicated BUSCOs (D)	   
	0	Fragmented BUSCOs (F)			   
	8	Missing BUSCOs (M)			   
	958	Total BUSCO groups searched		   

Dependencies and versions:
	hmmsearch: 3.1
	prodigal: 2.6.3
	sepp: 4.3.10

Placement file versions:
	list_of_reference_markers.archaea_odb10.2019-12-16.txt
	tree.archaea_odb10.2019-12-16.nwk
	tree_metadata.archaea_odb10.2019-12-16.txt
	supermatrix.aln.archaea_odb10.2019-12-16.faa
	mapping_taxids-busco_dataset_name.archaea_odb10.2019-12-16.txt
	mapping_taxid-lineage.archaea_odb10.2019-12-16.txt
