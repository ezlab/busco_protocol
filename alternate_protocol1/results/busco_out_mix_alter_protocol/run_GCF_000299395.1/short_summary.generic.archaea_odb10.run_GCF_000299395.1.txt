# BUSCO version is: 5.2.2 
# The lineage dataset is: archaea_odb10 (Creation date: 2021-02-23, number of genomes: 404, number of BUSCOs: 194)
# Summarized benchmarking in BUSCO notation for file /data/manni/BUSCO_PROTOCOL/busco_protocol/alternate_protocol1/genomes_mix/GCF_000299395.1.fna
# BUSCO was run in mode: genome
# Gene predictor used: prodigal

	***** Results: *****

	C:89.7%[S:89.2%,D:0.5%],F:1.5%,M:8.8%,n:194	   
	174	Complete BUSCOs (C)			   
	173	Complete and single-copy BUSCOs (S)	   
	1	Complete and duplicated BUSCOs (D)	   
	3	Fragmented BUSCOs (F)			   
	17	Missing BUSCOs (M)			   
	194	Total BUSCO groups searched		   

Dependencies and versions:
	hmmsearch: 3.1
	prodigal: 2.6.3
