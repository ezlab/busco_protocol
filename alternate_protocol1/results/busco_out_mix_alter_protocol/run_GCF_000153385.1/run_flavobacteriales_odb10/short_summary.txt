# BUSCO version is: 5.2.2 
# The lineage dataset is: flavobacteriales_odb10 (Creation date: 2021-02-23, number of genomes: 213, number of BUSCOs: 733)
# Summarized benchmarking in BUSCO notation for file /data/manni/BUSCO_PROTOCOL/busco_protocol/alternate_protocol1/genomes_mix/GCF_000153385.1.fna
# BUSCO was run in mode: genome
# Gene predictor used: prodigal

	***** Results: *****

	C:99.5%[S:99.5%,D:0.0%],F:0.4%,M:0.1%,n:733	   
	729	Complete BUSCOs (C)			   
	729	Complete and single-copy BUSCOs (S)	   
	0	Complete and duplicated BUSCOs (D)	   
	3	Fragmented BUSCOs (F)			   
	1	Missing BUSCOs (M)			   
	733	Total BUSCO groups searched		   

Dependencies and versions:
	hmmsearch: 3.1
	prodigal: 2.6.3
	sepp: 4.3.10

Placement file versions:
	list_of_reference_markers.bacteria_odb10.2019-12-16.txt
	tree.bacteria_odb10.2019-12-16.nwk
	tree_metadata.bacteria_odb10.2019-12-16.txt
	supermatrix.aln.bacteria_odb10.2019-12-16.faa
	mapping_taxids-busco_dataset_name.bacteria_odb10.2019-12-16.txt
	mapping_taxid-lineage.bacteria_odb10.2019-12-16.txt
