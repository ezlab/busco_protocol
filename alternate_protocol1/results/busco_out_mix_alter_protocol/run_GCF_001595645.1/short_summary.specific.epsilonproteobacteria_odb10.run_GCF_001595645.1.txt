# BUSCO version is: 5.2.2 
# The lineage dataset is: epsilonproteobacteria_odb10 (Creation date: 2020-03-06, number of genomes: 109, number of BUSCOs: 591)
# Summarized benchmarking in BUSCO notation for file /data/manni/BUSCO_PROTOCOL/busco_protocol/alternate_protocol1/genomes_mix/GCF_001595645.1.fna
# BUSCO was run in mode: genome
# Gene predictor used: prodigal

	***** Results: *****

	C:94.1%[S:93.6%,D:0.5%],F:0.7%,M:5.2%,n:591	   
	556	Complete BUSCOs (C)			   
	553	Complete and single-copy BUSCOs (S)	   
	3	Complete and duplicated BUSCOs (D)	   
	4	Fragmented BUSCOs (F)			   
	31	Missing BUSCOs (M)			   
	591	Total BUSCO groups searched		   

Dependencies and versions:
	hmmsearch: 3.1
	prodigal: 2.6.3
	sepp: 4.3.10

Placement file versions:
	list_of_reference_markers.bacteria_odb10.2019-12-16.txt
	tree.bacteria_odb10.2019-12-16.nwk
	tree_metadata.bacteria_odb10.2019-12-16.txt
	supermatrix.aln.bacteria_odb10.2019-12-16.faa
	mapping_taxids-busco_dataset_name.bacteria_odb10.2019-12-16.txt
	mapping_taxid-lineage.bacteria_odb10.2019-12-16.txt
