# BUSCO version is: 5.2.2 
# The lineage dataset is: bacillales_odb10 (Creation date: 2021-02-23, number of genomes: 412, number of BUSCOs: 450)
# Summarized benchmarking in BUSCO notation for file /data/manni/BUSCO_PROTOCOL/busco_protocol/alternate_protocol1/genomes_mix/GCF_000974285.1.fna
# BUSCO was run in mode: genome
# Gene predictor used: prodigal

	***** Results: *****

	C:98.6%[S:98.2%,D:0.4%],F:0.0%,M:1.4%,n:450	   
	444	Complete BUSCOs (C)			   
	442	Complete and single-copy BUSCOs (S)	   
	2	Complete and duplicated BUSCOs (D)	   
	0	Fragmented BUSCOs (F)			   
	6	Missing BUSCOs (M)			   
	450	Total BUSCO groups searched		   

Dependencies and versions:
	hmmsearch: 3.1
	prodigal: 2.6.3
	sepp: 4.3.10

Placement file versions:
	list_of_reference_markers.bacteria_odb10.2019-12-16.txt
	tree.bacteria_odb10.2019-12-16.nwk
	tree_metadata.bacteria_odb10.2019-12-16.txt
	supermatrix.aln.bacteria_odb10.2019-12-16.faa
	mapping_taxids-busco_dataset_name.bacteria_odb10.2019-12-16.txt
	mapping_taxid-lineage.bacteria_odb10.2019-12-16.txt
