# BUSCO version is: 5.2.2 
# The lineage dataset is: desulfurococcales_odb10 (Creation date: 2021-02-23, number of genomes: 13, number of BUSCOs: 491)
# Summarized benchmarking in BUSCO notation for file /data/manni/BUSCO_PROTOCOL/busco_protocol/alternate_protocol1/genomes_mix/GCF_000020905.1.fna
# BUSCO was run in mode: genome
# Gene predictor used: prodigal

	***** Results: *****

	C:96.9%[S:96.9%,D:0.0%],F:0.2%,M:2.9%,n:491	   
	476	Complete BUSCOs (C)			   
	476	Complete and single-copy BUSCOs (S)	   
	0	Complete and duplicated BUSCOs (D)	   
	1	Fragmented BUSCOs (F)			   
	14	Missing BUSCOs (M)			   
	491	Total BUSCO groups searched		   

Dependencies and versions:
	hmmsearch: 3.1
	prodigal: 2.6.3
	sepp: 4.3.10

Placement file versions:
	list_of_reference_markers.archaea_odb10.2019-12-16.txt
	tree.archaea_odb10.2019-12-16.nwk
	tree_metadata.archaea_odb10.2019-12-16.txt
	supermatrix.aln.archaea_odb10.2019-12-16.faa
	mapping_taxids-busco_dataset_name.archaea_odb10.2019-12-16.txt
	mapping_taxid-lineage.archaea_odb10.2019-12-16.txt
