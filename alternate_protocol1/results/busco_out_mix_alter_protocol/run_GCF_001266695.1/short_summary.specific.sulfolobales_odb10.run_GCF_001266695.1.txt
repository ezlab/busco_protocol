# BUSCO version is: 5.2.2 
# The lineage dataset is: sulfolobales_odb10 (Creation date: 2021-02-23, number of genomes: 13, number of BUSCOs: 1244)
# Summarized benchmarking in BUSCO notation for file /data/manni/BUSCO_PROTOCOL/busco_protocol/alternate_protocol1/genomes_mix/GCF_001266695.1.fna
# BUSCO was run in mode: genome
# Gene predictor used: prodigal

	***** Results: *****

	C:98.2%[S:98.0%,D:0.2%],F:0.3%,M:1.5%,n:1244	   
	1222	Complete BUSCOs (C)			   
	1219	Complete and single-copy BUSCOs (S)	   
	3	Complete and duplicated BUSCOs (D)	   
	4	Fragmented BUSCOs (F)			   
	18	Missing BUSCOs (M)			   
	1244	Total BUSCO groups searched		   

Dependencies and versions:
	hmmsearch: 3.1
	prodigal: 2.6.3
	sepp: 4.3.10

Placement file versions:
	list_of_reference_markers.archaea_odb10.2019-12-16.txt
	tree.archaea_odb10.2019-12-16.nwk
	tree_metadata.archaea_odb10.2019-12-16.txt
	supermatrix.aln.archaea_odb10.2019-12-16.faa
	mapping_taxids-busco_dataset_name.archaea_odb10.2019-12-16.txt
	mapping_taxid-lineage.archaea_odb10.2019-12-16.txt
