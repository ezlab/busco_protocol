# BUSCO version is: 5.2.2 
# The lineage dataset is: methanococcales_odb10 (Creation date: 2021-02-23, number of genomes: 18, number of BUSCOs: 958)
# Summarized benchmarking in BUSCO notation for file /data/manni/BUSCO_PROTOCOL/busco_protocol/alternate_protocol1/genomes_mix/GCF_000363885.1.fna
# BUSCO was run in mode: genome
# Gene predictor used: prodigal

	***** Results: *****

	C:90.6%[S:90.6%,D:0.0%],F:2.4%,M:7.0%,n:958	   
	868	Complete BUSCOs (C)			   
	868	Complete and single-copy BUSCOs (S)	   
	0	Complete and duplicated BUSCOs (D)	   
	23	Fragmented BUSCOs (F)			   
	67	Missing BUSCOs (M)			   
	958	Total BUSCO groups searched		   

Dependencies and versions:
	hmmsearch: 3.1
	prodigal: 2.6.3
	sepp: 4.3.10

Placement file versions:
	list_of_reference_markers.archaea_odb10.2019-12-16.txt
	tree.archaea_odb10.2019-12-16.nwk
	tree_metadata.archaea_odb10.2019-12-16.txt
	supermatrix.aln.archaea_odb10.2019-12-16.faa
	mapping_taxids-busco_dataset_name.archaea_odb10.2019-12-16.txt
	mapping_taxid-lineage.archaea_odb10.2019-12-16.txt
