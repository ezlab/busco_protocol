# BUSCO version is: 5.2.2 
# The lineage dataset is: entomoplasmatales_odb10 (Creation date: 2020-03-06, number of genomes: 25, number of BUSCOs: 332)
# Summarized benchmarking in BUSCO notation for file /data/manni/BUSCO_PROTOCOL/busco_protocol/alternate_protocol1/genomes_mix/GCF_000820525.2.fna
# BUSCO was run in mode: genome
# Gene predictor used: prodigal

	***** Results: *****

	C:95.5%[S:92.8%,D:2.7%],F:1.5%,M:3.0%,n:332	   
	317	Complete BUSCOs (C)			   
	308	Complete and single-copy BUSCOs (S)	   
	9	Complete and duplicated BUSCOs (D)	   
	5	Fragmented BUSCOs (F)			   
	10	Missing BUSCOs (M)			   
	332	Total BUSCO groups searched		   

Dependencies and versions:
	hmmsearch: 3.1
	prodigal: 2.6.3
