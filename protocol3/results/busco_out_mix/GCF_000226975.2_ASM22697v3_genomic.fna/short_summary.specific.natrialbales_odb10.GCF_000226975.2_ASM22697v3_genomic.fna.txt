# BUSCO version is: 5.2.2 
# The lineage dataset is: natrialbales_odb10 (Creation date: 2021-02-23, number of genomes: 49, number of BUSCOs: 1368)
# Summarized benchmarking in BUSCO notation for file /data/manni/BUSCO_PROTOCOL/busco_protocol/protocol3/genomes_mix/GCF_000226975.2_ASM22697v3_genomic.fna
# BUSCO was run in mode: genome
# Gene predictor used: prodigal

	***** Results: *****

	C:98.7%[S:98.3%,D:0.4%],F:0.3%,M:1.0%,n:1368	   
	1351	Complete BUSCOs (C)			   
	1345	Complete and single-copy BUSCOs (S)	   
	6	Complete and duplicated BUSCOs (D)	   
	4	Fragmented BUSCOs (F)			   
	13	Missing BUSCOs (M)			   
	1368	Total BUSCO groups searched		   

Dependencies and versions:
	hmmsearch: 3.1
	prodigal: 2.6.3
	metaeuk: 4.a0f584d
	sepp: 4.3.10

Placement file versions:
	list_of_reference_markers.archaea_odb10.2019-12-16.txt
	tree.archaea_odb10.2019-12-16.nwk
	tree_metadata.archaea_odb10.2019-12-16.txt
	supermatrix.aln.archaea_odb10.2019-12-16.faa
	mapping_taxids-busco_dataset_name.archaea_odb10.2019-12-16.txt
	mapping_taxid-lineage.archaea_odb10.2019-12-16.txt
