# BUSCO version is: 5.2.2 
# The lineage dataset is: archaea_odb10 (Creation date: 2021-02-23, number of genomes: 404, number of BUSCOs: 194)
# Summarized benchmarking in BUSCO notation for file /data/manni/BUSCO_PROTOCOL/busco_protocol/protocol3/genomes_mix/GCA_900036045.1_Methanoculleus_sp_MAB1_genomic.fna
# BUSCO was run in mode: genome
# Gene predictor used: prodigal

	***** Results: *****

	C:88.6%[S:88.1%,D:0.5%],F:7.2%,M:4.2%,n:194	   
	172	Complete BUSCOs (C)			   
	171	Complete and single-copy BUSCOs (S)	   
	1	Complete and duplicated BUSCOs (D)	   
	14	Fragmented BUSCOs (F)			   
	8	Missing BUSCOs (M)			   
	194	Total BUSCO groups searched		   

Dependencies and versions:
	hmmsearch: 3.1
	prodigal: 2.6.3
	metaeuk: 4.a0f584d
	sepp: 4.3.10
