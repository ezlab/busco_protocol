# BUSCO version is: 5.2.2 
# The lineage dataset is: bacteria_odb10 (Creation date: 2020-03-06, number of genomes: 4085, number of BUSCOs: 124)
# Summarized benchmarking in BUSCO notation for file /data/manni/BUSCO_PROTOCOL/busco_protocol/protocol3/genomes_mix/GCA_003366055.1_ASM336605v1_genomic.fna
# BUSCO was run in mode: genome
# Gene predictor used: prodigal

	***** Results: *****

	C:63.7%[S:63.7%,D:0.0%],F:8.9%,M:27.4%,n:124	   
	79	Complete BUSCOs (C)			   
	79	Complete and single-copy BUSCOs (S)	   
	0	Complete and duplicated BUSCOs (D)	   
	11	Fragmented BUSCOs (F)			   
	34	Missing BUSCOs (M)			   
	124	Total BUSCO groups searched		   

Dependencies and versions:
	hmmsearch: 3.1
	prodigal: 2.6.3
	metaeuk: 4.a0f584d
	sepp: 4.3.10
