# BUSCO version is: 5.2.2 
# The lineage dataset is: legionellales_odb10 (Creation date: 2020-03-06, number of genomes: 43, number of BUSCOs: 772)
# Summarized benchmarking in BUSCO notation for file /data/manni/BUSCO_PROTOCOL/busco_protocol/protocol3/genomes_mix/GCA_900452565.1_34347_D01_genomic.fna
# BUSCO was run in mode: genome
# Gene predictor used: prodigal

	***** Results: *****

	C:95.6%[S:95.6%,D:0.0%],F:2.6%,M:1.8%,n:772	   
	738	Complete BUSCOs (C)			   
	738	Complete and single-copy BUSCOs (S)	   
	0	Complete and duplicated BUSCOs (D)	   
	20	Fragmented BUSCOs (F)			   
	14	Missing BUSCOs (M)			   
	772	Total BUSCO groups searched		   

Dependencies and versions:
	hmmsearch: 3.1
	prodigal: 2.6.3
	metaeuk: 4.a0f584d
	sepp: 4.3.10

Placement file versions:
	list_of_reference_markers.bacteria_odb10.2019-12-16.txt
	tree.bacteria_odb10.2019-12-16.nwk
	tree_metadata.bacteria_odb10.2019-12-16.txt
	supermatrix.aln.bacteria_odb10.2019-12-16.faa
	mapping_taxids-busco_dataset_name.bacteria_odb10.2019-12-16.txt
	mapping_taxid-lineage.bacteria_odb10.2019-12-16.txt
