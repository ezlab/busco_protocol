# BUSCO version is: 5.2.2 
# The lineage dataset is: chlorophyta_odb10 (Creation date: 2020-08-05, number of genomes: 16, number of BUSCOs: 1519)
# Summarized benchmarking in BUSCO notation for file /data/manni/BUSCO_PROTOCOL/busco_protocol/protocol3/genomes_mix/GCF_002220235.1_ASM222023v1_genomic.fna
# BUSCO was run in mode: genome
# Gene predictor used: metaeuk

	***** Results: *****

	C:97.1%[S:96.7%,D:0.4%],F:0.7%,M:2.2%,n:1519	   
	1475	Complete BUSCOs (C)			   
	1469	Complete and single-copy BUSCOs (S)	   
	6	Complete and duplicated BUSCOs (D)	   
	10	Fragmented BUSCOs (F)			   
	34	Missing BUSCOs (M)			   
	1519	Total BUSCO groups searched		   

Dependencies and versions:
	hmmsearch: 3.1
	prodigal: 2.6.3
	metaeuk: 4.a0f584d
	sepp: 4.3.10

Placement file versions:
	list_of_reference_markers.eukaryota_odb10.2019-12-16.txt
	tree.eukaryota_odb10.2019-12-16.nwk
	tree_metadata.eukaryota_odb10.2019-12-16.txt
	supermatrix.aln.eukaryota_odb10.2019-12-16.faa
	mapping_taxids-busco_dataset_name.eukaryota_odb10.2019-12-16.txt
	mapping_taxid-lineage.eukaryota_odb10.2019-12-16.txt
