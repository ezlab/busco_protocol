# BUSCO version is: 5.2.2 
# The lineage dataset is: saccharomycetes_odb10 (Creation date: 2020-08-05, number of genomes: 76, number of BUSCOs: 2137)
# Summarized benchmarking in BUSCO notation for file /data/manni/BUSCO_PROTOCOL/busco_protocol/protocol3/genomes_mix/GCF_000182965.3_ASM18296v3_genomic.fna
# BUSCO was run in mode: genome
# Gene predictor used: metaeuk

	***** Results: *****

	C:98.6%[S:98.0%,D:0.6%],F:0.8%,M:0.6%,n:2137	   
	2108	Complete BUSCOs (C)			   
	2095	Complete and single-copy BUSCOs (S)	   
	13	Complete and duplicated BUSCOs (D)	   
	17	Fragmented BUSCOs (F)			   
	12	Missing BUSCOs (M)			   
	2137	Total BUSCO groups searched		   

Dependencies and versions:
	hmmsearch: 3.1
	prodigal: 2.6.3
	metaeuk: 4.a0f584d
	sepp: 4.3.10

Placement file versions:
	list_of_reference_markers.eukaryota_odb10.2019-12-16.txt
	tree.eukaryota_odb10.2019-12-16.nwk
	tree_metadata.eukaryota_odb10.2019-12-16.txt
	supermatrix.aln.eukaryota_odb10.2019-12-16.faa
	mapping_taxids-busco_dataset_name.eukaryota_odb10.2019-12-16.txt
	mapping_taxid-lineage.eukaryota_odb10.2019-12-16.txt
