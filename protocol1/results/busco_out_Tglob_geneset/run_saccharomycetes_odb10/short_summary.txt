# BUSCO version is: 5.2.2 
# The lineage dataset is: saccharomycetes_odb10 (Creation date: 2020-08-05, number of genomes: 76, number of BUSCOs: 2137)
# Summarized benchmarking in BUSCO notation for file /data/manni/BUSCO_PROTOCOL/busco_protocol/protocol1/Tglobosa_GCF_014133895.1_geneset.faa
# BUSCO was run in mode: proteins

	***** Results: *****

	C:99.1%[S:98.9%,D:0.2%],F:0.7%,M:0.2%,n:2137	   
	2117	Complete BUSCOs (C)			   
	2113	Complete and single-copy BUSCOs (S)	   
	4	Complete and duplicated BUSCOs (D)	   
	15	Fragmented BUSCOs (F)			   
	5	Missing BUSCOs (M)			   
	2137	Total BUSCO groups searched		   

Dependencies and versions:
	hmmsearch: 3.1
