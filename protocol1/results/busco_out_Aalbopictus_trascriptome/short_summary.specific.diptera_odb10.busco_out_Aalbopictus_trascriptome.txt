# BUSCO version is: 5.2.2 
# The lineage dataset is: diptera_odb10 (Creation date: 2020-08-05, number of genomes: 56, number of BUSCOs: 3285)
# Summarized benchmarking in BUSCO notation for file /data/manni/BUSCO_PROTOCOL/busco_protocol/protocol1/Aalbopictus_transcriptome.fna
# BUSCO was run in mode: transcriptome

	***** Results: *****

	C:38.4%[S:30.8%,D:7.6%],F:1.8%,M:59.8%,n:3285	   
	1264	Complete BUSCOs (C)			   
	1013	Complete and single-copy BUSCOs (S)	   
	251	Complete and duplicated BUSCOs (D)	   
	59	Fragmented BUSCOs (F)			   
	1962	Missing BUSCOs (M)			   
	3285	Total BUSCO groups searched		   

Dependencies and versions:
	hmmsearch: 3.1
	metaeuk: 4.a0f584d
