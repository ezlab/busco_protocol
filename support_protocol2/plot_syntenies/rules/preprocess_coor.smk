rule preprocess_coor:
    input:
        tables = config['full_table_path'] + "/{sample}" + "_full_table.tsv"
    output:
        coordinates = config["out_path"] + "/{sample}_busco_coordinates.txt"
    params:
        karyotype_path = config["karyotype_path"],
        geno_path = config["geno_path"],
        geno_suff = config["geno_suff"],
        selected_seqs = config["selected_seqs"],
        out_path = config["out_path"]
    conda:
        config["conda_env"]
    threads: config["seqkit_threads"]
    shell:
        """
        set -x
        set -e

        grep -v "#" {input.tables} | cut -f 1,2,3,4,5  | grep -v "Missing"  > {params.out_path}/{wildcards.sample}_busco_coordinates.txt
        
        if [ ! {params.karyotype_path} ];
        then

        # if select_sequences is passed, select only subset of seqs
        if [ ! {params.selected_seqs} ];
        then
        seqkit fx2tab -i -n -l {params.geno_path}/{wildcards.sample}{params.geno_suff}  > {params.out_path}/{wildcards.sample}_for_karyotype.txt
        else
        seqkit fx2tab -i -n -l {params.geno_path}/{wildcards.sample}{params.geno_suff} | grep -f {params.selected_seqs}/{wildcards.sample}_selected_seqsID.txt  > {params.out_path}/{wildcards.sample}_for_karyotype.txt
        fi

        Rscript scripts/plot_synteny1.R {params.out_path}/{wildcards.sample}_for_karyotype.txt {wildcards.sample}
        fi
        """
