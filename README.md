# BUSCO_protocol repo for:  
Manni M, Berkeley MR, Seppey M, Zdobnov EM. 2021. BUSCO: Assessing Genomic Data Quality and Beyond. Current Protocols 1:e323.
https://currentprotocols.onlinelibrary.wiley.com/doi/full/10.1002/cpz1.323


Note: There's a small typo in the code of section "Support Protocol 3: BUILDING PHYLOGENOMIC TREES":  
`$ snakemake –cores 16 --use-conda` should be:
`$ snakemake --cores 16 --use-conda`


In case you have issues running snakemake with mamba use the following command:   
`snakemake --cores 16 --use-conda --conda-frontend conda`

<br/> 
<br/>
<br/>

**Visualizing BUSCO markers on genomes (Support protocol 2):**
<figure>
  <img src="images/Figure2.png" alt="markers" width=400">
  <figcaption>Example of plotting the location of BUSCO markers on a genome sequence. The figure shows D. melanogaster nuclear chromosomes along with the position of complete (light blue), duplicated (black), and fragmented (yellow) BUSCO markers from the diptera_odb10 dataset. The Y sex chromosome does not harbour BUSCO markers.</figcaption>
</figure>

<br/>
<br/>

**Visualizing syntenies between genomes using BUSCO markers (Support protocol 2):**
<figure>
  <img src="images/Figure3.png" alt="markers" width=400">
  <figcaption>Synteny  between the D. melanogaster and D. pseudoobscura sequenced chromosomes, computed by mapping the location of the diptera_odb10 markers obtained by running BUSCO on both genomes.</figcaption>
</figure>

<br/> 
<br/>

**Building phylogenomic tree from a set of genome assemblies and GFF files (Support protocol 3):**
<figure>
  <img src="images/Figure4.png" alt="markers" width=400">
  <figcaption>Example of a phylogenomic tree obtained by using single-copy orthologs identified by BUSCO on a test data of six insect species. The phylogeny was inferred from a set of 1154 concatenated BUSCOs under the Q.insect+R5 substitution model using IQ-TREE 2.</figcaption>
</figure>












