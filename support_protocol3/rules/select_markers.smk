Nsamples=len(SAMPLES)

rule select_markers:
    input: directory(config["out_path"] + "/run_{sample}")
    output: config["out_path"] + "/" + config["dataset"] + "_{sample}_full_table_edited.txt"
    params:
        out_path = config["out_path"],
        dataset = config["dataset"],
    shell:
        """
        set -x
        set -e

        sleep $((RANDOM % 10))

        grep -v "^#" {input}/run_{params.dataset}/full_table.tsv | sed "s/^/{wildcards.sample}\t/g" | sed 's/Missing/Missing\t-\t-\t-/' > {output}

        """
