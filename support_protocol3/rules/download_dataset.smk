rule download:
    input:
    output:
        busco_downloads = directory("busco_downloads")
    params:
        out_path = config["out_path"],
        dataset = config["dataset"]
    conda:
        config["conda_env3"]
    threads: config["threads"]
    shell:
        """
        set -x
        set -e

        # run BUSCO on filtered gene sets:
        busco --download {params.dataset} 
        """
