rule filter_isoforms:
    input:
        GFF = config['path_to_GFFs'] + "/{sample}" + ".gff",
        genome = config['path_to_genomes'] + "/{sample}" + ".fna"
    output:
        gene_set_filtered_gff = config["out_gene_set_filtered"] + "/{sample}_filtered.gff",
        gene_set_filtered_aa = config["out_gene_set_filtered"] + "/{sample}_filtered.faa"
    params:
        geno_suff = config["geno_suff"],
        out_gene_set_filtered = config["out_gene_set_filtered"],
        threads = config["threads"]
    conda:
        config["conda_env1"]
    log: log1="logs/filtering_{sample}.log"
    threads: 1
    shell:
        """
        set -x
        set -e

        # filter using the gff
        agat_sp_keep_longest_isoform.pl -gff {input.GFF} -o {output.gene_set_filtered_gff} &> {log.log1}

        # extract protein from filtered GFF and genome
        agat_sp_extract_sequences.pl -g {output.gene_set_filtered_gff} -f {input.genome} -p -o {output.gene_set_filtered_aa} --clean_final_stop

        # generate stats on filtered gene sets:
        seqkit stats {output.gene_set_filtered_aa} > {params.out_gene_set_filtered}/stats_{wildcards.sample}_geneset_filtered.txt
        """
