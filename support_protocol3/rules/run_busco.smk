
Nsamples=len(SAMPLES)

rule run_busco:
    input:
        gene_set_filtered_aa = config["out_gene_set_filtered"] + "/{sample}_filtered.faa",
        busco_downloads = directory("busco_downloads")
    output:
        busco_result = directory(config["out_path"] + "/run_{sample}") 
    params:
        out_path = config["out_path"],
        dataset = config["dataset"],
        percent_missing = config["percent_missing"],
        keep_duplicates = config["keep_duplicates"]
    conda:
        config["conda_env3"]
    threads: config["threads"]
    shell:
        """
        set -x
        set -e

        # run BUSCO on filtered gene sets:
        busco -i {input.gene_set_filtered_aa} -l {params.dataset} -o run_{wildcards.sample} -m prot -c {threads} --offline --out_path {params.out_path}
        """
