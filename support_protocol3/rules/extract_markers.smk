Nsamples=len(SAMPLES)

rule extract_markers:
    input:
        expand("data/extracted/{sample}", sample=SAMPLES)
    output:
        config["out_path"] + "/superalignment_oneline.fa"
    params:
        out_path = config["out_path"],
        dataset = config["dataset"],
    conda:
        config["conda_env1"]
    threads: config["threads"]
    shell:
        """
        set -x
        set -e

        # Run mafft and trimAl on each BUSCO family
        echo "mafft+trimAl..."
        for b in $(ls data/extracted/all/*)
        do
        echo ${{b}}
        mafft --thread -{threads} --auto ${{b}} > "${{b}}.aln"
        trimal -in "${{b}}.aln" -out "${{b}}.aln.trimmed" -automated1
        done

        # run AMAS for concatenation
        AMAS.py concat -f fasta -d aa -i data/extracted/all/*.aln.trimmed -t {params.out_path}/superalignment.fa -p {params.out_path}/superalignment_partitions.txt --part-format raxml

        # summary stats of superalign
        AMAS.py summary -f fasta -d aa -i {params.out_path}/superalignment.fa -o {params.out_path}/superalignment_stats.txt

        # oneline fasta
        seqkit seq -w0 {params.out_path}/superalignment.fa > {params.out_path}/superalignment_oneline.fa
        """

