Nsamples=len(SAMPLES)

rule select_markers2:
    input: expand(config["out_path"] + "/" + config["dataset"] + "_{sample}_full_table_edited.txt", sample=SAMPLES)
    output: 
        config["out_path"] + "/" + config["dataset"] + "_BUSCOs_all_species.txt",
        config["out_path"] + "/" + "selected_markers.txt"
    params:
        out_path = config["out_path"],
        dataset = config["dataset"],
        percent_missing = config["percent_missing"],
        keep_duplicates = config["keep_duplicates"],
        out_gene_set_filtered = config["out_gene_set_filtered"]
    conda:
        config["conda_env2"]
    shell:
        """
        set -x
        set -e

        cat {input} | cut -f 1,2,3,4,5,6 > {params.out_path}/{params.dataset}_BUSCOs_all_species.txt
        Rscript scripts/select_markers.R {params.out_path}/{params.dataset}_BUSCOs_all_species.txt {params.percent_missing} {params.keep_duplicates} {params.out_path}
        mv {params.out_path}/*_selected_markers_* {params.out_path}/selected_markers.txt
        """
