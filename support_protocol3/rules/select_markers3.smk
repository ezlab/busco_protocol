Nsamples=len(SAMPLES)

rule select_markers3:
    input: 
        config["out_path"] + "/" + "selected_markers.txt",
        expand(config["out_path"] + "/" + "run_{sample}", sample=SAMPLES)
    output:
        "data/extracted/{sample}_log.txt", 
        directory("data/extracted/{sample}")
    params:
        out_path = config["out_path"],
        dataset = config["dataset"]
    conda:
        config["conda_env1"]
    shell:
        """
        set -x
        set -e

        if [[ ! -e ./data/extracted/all ]]; then mkdir -p ./data/extracted/all; fi
        if [[ ! -e ./data/extracted/{wildcards.sample} ]]; then mkdir -p ./data/extracted/{wildcards.sample}; fi

        for BUSCO in `cat {input[0]}`; do cat $(find {params.out_path}/run_{wildcards.sample}/run_{params.dataset}/busco_sequences/ -name "${{BUSCO}}.faa") | seqkit seq -w0 | sed "s/^>.*/>{wildcards.sample}/g" | head -n 2 | tee -a {output}/${{BUSCO}}.faa data/extracted/all/${{BUSCO}}.faa  >/dev/null; done; echo "{wildcards.sample} DONE" > ./data/extracted/{wildcards.sample}_log.txt
        """
