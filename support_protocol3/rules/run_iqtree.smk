

rule run_iqtree:
    input:
        config["out_path"] + "/superalignment_oneline.fa"
    output:
        config["out_path"] + "/phylogeny.treefile"
    params:
        out_path = config["out_path"],
        dataset = config["dataset"],
    conda:
        config["conda_env1"] 
    threads: config["threads"]
    shell:
        """
        set -x
        set -e

        # run BUSCO on filtered gene sets:
        iqtree2 -m Q.insect+R5 -s {params.out_path}/superalignment.fa -o Zootermopsis_nevadensis -pre "{params.out_path}/phylogeny" -B 1000 -T {threads}
	"""
